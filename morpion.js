var inputEl = document.getElementById("input");
var startBtnEl = document.querySelector("button[name='startGame']");
let boxes = document.querySelectorAll(".case");

inputEl.addEventListener("input", function() {   
    activateButton();
});

function activateButton(){
    if (inputEl.value != "") {
	startBtnEl.disabled = false;
	startBtnEl.style.display = "block";
    } else {
	startBtnEl.disabled = true;
	startBtnEl.style.display = "none";
    }
}

// https://stackoverflow.com/questions/951021/what-is-the-javascript-version-of-sleep
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// ######                              
// #     #  ####    ##   #####  #####  
// #     # #    #  #  #  #    # #    # 
// ######  #    # #    # #    # #    # 
// #     # #    # ###### #####  #    # 
// #     # #    # #    # #   #  #    # 
// ######   ####  #    # #    # #####  

let Board = class Board {

    constructor() {
	this.winningCombis = [
	    [1, 2, 3],
	    [4, 5, 6],
	    [7, 8, 9],
	    [7, 5, 3],
	    [1, 5, 9],
	    [1, 4, 7],
	    [2, 5, 8],
	    [3, 6, 9]
	];

	this.boxes = [];
	for (var i = 0; i < 9; i++) {
	    this.boxes[i] = undefined;
	}
    }

    startGame(nameHumanPlayer, nameRobotPlayer) {
	boxes.forEach((box) => {
	    box.addEventListener('click', () => {
		if ( this.boxes[box.dataset.box] == undefined ) {
	    	    this.play(nameHumanPlayer, box.dataset.box);
		    let playBot = async () => {
			await sleep(600);			
			this.play(nameRobotPlayer, this.generateMove());
		    }
		    playBot();
		}
	    });
	});
    }

    play(namePlayer,boxPosition){
	this.checkBox(namePlayer, boxPosition);
	    if (this.isPlayerHasAWinningCombi(namePlayer))
	    {
		window.alert(namePlayer + " Won !");
		location.reload(); // Pas compris (précisément)
				   // pourquoi quand je lance un
				   // nouveau jeu, les gagnants
				   // précédents sont aussi
				   // affichés. Ceci évite cela. ^^
	    }
    } 

    checkBox(namePlayer,boxPosition) {
	let cross;
	let box = document.querySelector("div[data-box='" + boxPosition + "']");
	this.boxes[boxPosition] = namePlayer;
	cross = (namePlayer === "Computer") ? 'O' : 'X';
	box.innerHTML = cross;
	box.classList.add("checked");
    }

    isPlayerHasAWinningCombi(nameHumanPlayer) {
	for (var i = 0; i < this.winningCombis.length; i++) {
	    if (this.isCombiWinning(this.winningCombis[i], nameHumanPlayer))
	    {
		// this.resetBoxes();
		return true;
	    }
	}
    }

    isCombiWinning(combi, nameHumanPlayer) {
	for (let i = 0; i < combi.length ; i++) {
	    if ( this.boxes[combi[i]] != nameHumanPlayer)
	    {
		break;
	    } else if ( i == 2) {
		return true;
	    }
	}
    }
	
    isBoxTaken(boxPosition) {
	if (this.boxes[boxPosition] != undefined){
	    return true;
	}
    }

    areThereFreeBoxes() {
	for (var i = 1; i < this.boxes.length; i++) {
	    if (this.boxes[i] == undefined) {
		return true;
	    } else if (i == this.boxes.length - 1) {
		return false;
	    }
	}
    }
    
    generateMove() {
	if (this.areThereFreeBoxes()) {
	    let boxPosition;
    	    do {
    		boxPosition = Math.floor((Math.random() * 9) + 1);
    	    } while ( this.isBoxTaken(boxPosition) );
    	    return boxPosition;
	} else {
	    alert("Nobody won !");
	    location.reload();
	}
    }
};

// ######                                    
// #     # #        ##   #   # ###### #####  
// #     # #       #  #   # #  #      #    # 
// ######  #      #    #   #   #####  #    # 
// #       #      ######   #   #      #####  
// #       #      #    #   #   #      #   #  
// #       ###### #    #   #   ###### #    # 


let Player = class Player {
    constructor(name) {
	this.name = name;
    }
};

startBtnEl.addEventListener("click", function() {
    startBtnEl.style.display = "none";
    document.getElementById("right-side").style.display = "none";
    startBtnEl.disabled = true;
    const humanPlayer = new Player(inputEl.value);
    const robotPlayer = new Player("Computer");
    const board = new Board();
    board.startGame(humanPlayer.name, robotPlayer.name);
});
